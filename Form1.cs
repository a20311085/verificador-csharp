﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Verificador_CSH
{
	public partial class Form1 : Form
	{
		string Ruta = "D:\\Escuela\\Arquitectura de Software\\Verificador CSH\\Resources";
		string Codigo = "";
		string[,] Productos =
		{
			{"1","RTX 4090","42,308$","Geforce de 24gb vram","producto1.png"},
			{"2","Gtx 1050","5,340 $","Asus de 4gb vram","producto2.png"},
			{"3","Rtx 2080","20,901 $","ASUS STRIX 12gb vram","producto3.png"},
			{"4","Ryzen 7 5800","5,203 $","AMD 3.90hz 8 core","producto4.png"},
			{"5","Ryzen 5 5600x","3,203$ $","AMD 3.70hz 6 core","producto5.png"}
		};

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			
			//label1.Location = new Point(this.Width / 2 - label1.Width / 2, 100);

			//pictureBox1.Location = new Point(this.Width / 2 - pictureBox1.Width / 2, this.Height / 2 - pictureBox1.Height / 2);

			//label2.Location = new Point(this.Width / 2 - label2.Width / 2, this.Height - label2.Height);
		}

		private void Form1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar != 13)
			{
				Codigo += e.KeyChar;
			}
			else
			{
				MessageBox.Show("Codigo " + Codigo);
				

				for (int i = 0; i  < 5; i++)
				{
					

					if (Codigo == Productos[i,0])
					{


						label3.Text = "Nombre " + Productos[i, 1];
						label3.Visible = true;

						label4.Text = "Precio " + Productos[i, 2];
						label4.Visible = true;

						label2.Text = "Descripción" + Productos[i, 3];
						label2.Visible = true;

						pictureBox1.Image = Image.FromFile(Ruta + @"\" + Productos[i, 4]);


					}

					
				}
				Codigo = "";
				
			}
		}

        private void button1_Click(object sender, EventArgs e)
        {
			this.Close();
        }
    }
}
